/*Write function parseF which takes an input and returns a number or null if conversion is not possible. The input can be one of many different types so be aware.*/
function parseF(s) {
    if(typeof(s) === 'number' || typeof(s) === 'string'){
      s = Number(s);
      if(isNaN(s) || typeof(s) === 'undefined'){
        return null;      
      }else{
        return s;
      }
    }else{
      return null;
    }
  }