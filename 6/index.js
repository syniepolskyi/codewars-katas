/*Write a function to convert a name into initials. This kata strictly takes two words with one space in between them.

The output should be two capital letters with a dot separating them.

It should look like this:

Sam Harris => S.H

patrick feeney => P.F*/


function abbrevName(name){
    let init ='';
      let name1 = name.split(' ');
    for(let i = 0; i < name1.length; i++){
      str = name1[i];
      if(i === 0){
        init = init + str[0].toUpperCase() + '.';
      }else{
        init = init + str[0].toUpperCase();
      } 
    }
    return init;
  }