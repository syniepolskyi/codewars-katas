/*Kevin is noticing his space run out! Write a function that removes the spaces from the values and returns an array showing the space decreasing. For example, running this function on the array ['i', 'have','no','space'] would produce ['i','ihave','ihaveno','ihavenospace'].*/

function spacey(array){
    let str='';
    let arr1 = [];
    for(let i = 0; i<array.length; i++){
      for(let j = 0; j<=i; j++){
        str = str + array[j];
      }
    arr1.push(str);
    str = '';
    }
    return arr1;
  }